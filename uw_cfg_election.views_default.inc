<?php

/**
 * @file
 * uw_cfg_election.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function uw_cfg_election_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'election_audit_trail';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'election_ballot';
  $view->human_name = 'Election audit trail';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Election audit trail';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'administer elections';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '100';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'uid' => 'uid',
    'timestamp' => 'timestamp',
    'ip' => 'ip',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'uid' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'timestamp' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'ip' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['style_options']['sticky'] = TRUE;
  $handler->display->display_options['style_options']['empty_table'] = TRUE;
  /* Relationship: Election ballot: Voter information */
  $handler->display->display_options['relationships']['uid']['id'] = 'uid';
  $handler->display->display_options['relationships']['uid']['table'] = 'election_ballot';
  $handler->display->display_options['relationships']['uid']['field'] = 'uid';
  /* Relationship: Election ballot: Election post information */
  $handler->display->display_options['relationships']['post_id']['id'] = 'post_id';
  $handler->display->display_options['relationships']['post_id']['table'] = 'election_ballot';
  $handler->display->display_options['relationships']['post_id']['field'] = 'post_id';
  /* Field: User: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'users';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['relationship'] = 'uid';
  /* Field: User: Username */
  $handler->display->display_options['fields']['username']['id'] = 'username';
  $handler->display->display_options['fields']['username']['table'] = 'users';
  $handler->display->display_options['fields']['username']['field'] = 'username';
  $handler->display->display_options['fields']['username']['relationship'] = 'uid';
  /* Field: Election ballot: Timestamp */
  $handler->display->display_options['fields']['timestamp']['id'] = 'timestamp';
  $handler->display->display_options['fields']['timestamp']['table'] = 'election_ballot';
  $handler->display->display_options['fields']['timestamp']['field'] = 'timestamp';
  $handler->display->display_options['fields']['timestamp']['date_format'] = 'short';
  $handler->display->display_options['fields']['timestamp']['second_date_format'] = 'long';
  /* Field: Election ballot: IP */
  $handler->display->display_options['fields']['ip']['id'] = 'ip';
  $handler->display->display_options['fields']['ip']['table'] = 'election_ballot';
  $handler->display->display_options['fields']['ip']['field'] = 'ip';
  $handler->display->display_options['fields']['ip']['label'] = 'IP address';
  /* Field: Election ballot: Post ID */
  $handler->display->display_options['fields']['post_id']['id'] = 'post_id';
  $handler->display->display_options['fields']['post_id']['table'] = 'election_ballot';
  $handler->display->display_options['fields']['post_id']['field'] = 'post_id';
  $handler->display->display_options['fields']['post_id']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['post_id']['alter']['path'] = 'election-post/[post_id]';
  /* Field: Election post: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'election_post';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['relationship'] = 'post_id';
  $handler->display->display_options['fields']['title']['label'] = 'Post title';
  $handler->display->display_options['fields']['title']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['path'] = 'election-post/[post_id]';
  /* Sort criterion: Election ballot: Timestamp */
  $handler->display->display_options['sorts']['timestamp']['id'] = 'timestamp';
  $handler->display->display_options['sorts']['timestamp']['table'] = 'election_ballot';
  $handler->display->display_options['sorts']['timestamp']['field'] = 'timestamp';
  /* Contextual filter: Election ballot: Election ID */
  $handler->display->display_options['arguments']['election_id']['id'] = 'election_id';
  $handler->display->display_options['arguments']['election_id']['table'] = 'election_ballot';
  $handler->display->display_options['arguments']['election_id']['field'] = 'election_id';
  $handler->display->display_options['arguments']['election_id']['default_action'] = 'not found';
  $handler->display->display_options['arguments']['election_id']['exception']['value'] = '';
  $handler->display->display_options['arguments']['election_id']['title_enable'] = TRUE;
  $handler->display->display_options['arguments']['election_id']['title'] = 'Election audit trail: %1';
  $handler->display->display_options['arguments']['election_id']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['election_id']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['election_id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['election_id']['summary_options']['items_per_page'] = '25';
  /* Filter criterion: User: Name */
  $handler->display->display_options['filters']['username']['id'] = 'username';
  $handler->display->display_options['filters']['username']['table'] = 'users';
  $handler->display->display_options['filters']['username']['field'] = 'username';
  $handler->display->display_options['filters']['username']['relationship'] = 'uid';
  $handler->display->display_options['filters']['username']['exposed'] = TRUE;
  $handler->display->display_options['filters']['username']['expose']['operator_id'] = 'username_op';
  $handler->display->display_options['filters']['username']['expose']['label'] = 'Username';
  $handler->display->display_options['filters']['username']['expose']['operator'] = 'username_op';
  $handler->display->display_options['filters']['username']['expose']['identifier'] = 'username';
  $handler->display->display_options['filters']['username']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    21 => 0,
    15 => 0,
    18 => 0,
    12 => 0,
    13 => 0,
    14 => 0,
    3 => 0,
    6 => 0,
    11 => 0,
    5 => 0,
    4 => 0,
    7 => 0,
    8 => 0,
    9 => 0,
    10 => 0,
    19 => 0,
    17 => 0,
    20 => 0,
    16 => 0,
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'election/%/audit-trail';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Audit trail';
  $handler->display->display_options['menu']['description'] = 'Audit trail';
  $handler->display->display_options['menu']['weight'] = '8';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;

  /* Display: Data export */
  $handler = $view->new_display('views_data_export', 'Data export', 'views_data_export_1');
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['style_plugin'] = 'views_data_export_csv';
  $handler->display->display_options['style_options']['provide_file'] = 0;
  $handler->display->display_options['style_options']['parent_sort'] = 0;
  $handler->display->display_options['style_options']['separator'] = '	';
  $handler->display->display_options['style_options']['quote'] = 0;
  $handler->display->display_options['style_options']['trim'] = 0;
  $handler->display->display_options['style_options']['replace_newlines'] = 1;
  $handler->display->display_options['style_options']['header'] = 1;
  $handler->display->display_options['style_options']['keep_html'] = 0;
  $handler->display->display_options['path'] = 'election/%/audit-trail.tsv';
  $translatables['election_audit_trail'] = array(
    t('Master'),
    t('Election audit trail'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« first'),
    t('‹ previous'),
    t('next ›'),
    t('last »'),
    t('Voter information'),
    t('Election post information'),
    t('Name'),
    t('Username'),
    t('Timestamp'),
    t('IP address'),
    t('Post ID'),
    t('.'),
    t(','),
    t('Post title'),
    t('All'),
    t('Election audit trail: %1'),
    t('Page'),
    t('Data export'),
  );
  $export['election_audit_trail'] = $view;

  return $export;
}
