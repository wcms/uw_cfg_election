uw_cfg_election 7.x-2.x is for elections in which the voters list is
stored in the Drupal site.

# Steps to setup a voting site

1. Build a site with the latest release of uw_base_profile 7.x-2.x and
this makefile:
https://git.uwaterloo.ca/wcms-sites/vote/

2. drush en uw_cfg_election

3. Visit: admin/config/system/uw_auth_cas_common_settings
Check "Enable open authentication" and save.

4. Visit: admin/structure/menu/manage/main-menu
Uncheck "Enabled" for each item and save.
Next to "Home" click "Delete" and confirm.

5. Visit: admin/config/election/candidate-types/manage/candidate/fields
Next to "E-mail address", click "Delete" and confirm.

6. Visit: admin/config/content/formats/add
In "Name", enter "filtered_html". Click "Save configuration".

7. Add and give site manager permission to those in charge of the site.
