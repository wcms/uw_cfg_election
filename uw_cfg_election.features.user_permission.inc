<?php

/**
 * @file
 * uw_cfg_election.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_cfg_election_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer candidate types'.
  $permissions['administer candidate types'] = array(
    'name' => 'administer candidate types',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'election_candidate',
  );

  // Exported permission: 'administer elections'.
  $permissions['administer elections'] = array(
    'name' => 'administer elections',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'election',
  );

  // Exported permission: 'bypass running election lock'.
  $permissions['bypass running election lock'] = array(
    'name' => 'bypass running election lock',
    'roles' => array(),
    'module' => 'election',
  );

  // Exported permission: 'create elections'.
  $permissions['create elections'] = array(
    'name' => 'create elections',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'election',
  );

  // Exported permission: 'delete any election'.
  $permissions['delete any election'] = array(
    'name' => 'delete any election',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'election',
  );

  // Exported permission: 'delete own elections'.
  $permissions['delete own elections'] = array(
    'name' => 'delete own elections',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'election',
  );

  // Exported permission: 'edit any election'.
  $permissions['edit any election'] = array(
    'name' => 'edit any election',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'election',
  );

  // Exported permission: 'edit own elections'.
  $permissions['edit own elections'] = array(
    'name' => 'edit own elections',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'election',
  );

  // Exported permission: 'edit own nominations'.
  $permissions['edit own nominations'] = array(
    'name' => 'edit own nominations',
    'roles' => array(),
    'module' => 'election_candidate',
  );

  // Exported permission: 'submit nominations'.
  $permissions['submit nominations'] = array(
    'name' => 'submit nominations',
    'roles' => array(),
    'module' => 'election_candidate',
  );

  // Exported permission: 'undo own vote'.
  $permissions['undo own vote'] = array(
    'name' => 'undo own vote',
    'roles' => array(),
    'module' => 'election_vote',
  );

  // Exported permission: 'view any election results'.
  $permissions['view any election results'] = array(
    'name' => 'view any election results',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'election_results',
  );

  // Exported permission: 'view election statistics'.
  $permissions['view election statistics'] = array(
    'name' => 'view election statistics',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'election_statistics',
  );

  // Exported permission: 'view election statistics block'.
  $permissions['view election statistics block'] = array(
    'name' => 'view election statistics block',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'election_statistics',
  );

  // Exported permission: 'view own election results'.
  $permissions['view own election results'] = array(
    'name' => 'view own election results',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'election_results',
  );

  // Exported permission: 'view published elections'.
  $permissions['view published elections'] = array(
    'name' => 'view published elections',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'election',
  );

  // Exported permission: 'view voting access explanation'.
  $permissions['view voting access explanation'] = array(
    'name' => 'view voting access explanation',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'election_vote',
  );

  // Exported permission: 'vote in elections'.
  $permissions['vote in elections'] = array(
    'name' => 'vote in elections',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'election_vote',
  );

  return $permissions;
}
