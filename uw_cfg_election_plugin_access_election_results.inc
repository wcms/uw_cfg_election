<?php

/**
 * @file
 * Definition of views_plugin_access_election_results.
 */

/**
 * Access plugin that provides access control based on uw_cfg_election_results_access_all().
 *
 * @ingroup views_access_plugins
 */
class uw_cfg_election_plugin_access_election_results extends views_plugin_access {

  /**
   *
   */
  public function access($account) {
    return uw_cfg_election_results_access_all('view', 1, $account);
  }

  /**
   *
   */
  public function get_access_callback() {
    return array('uw_cfg_election_results_access_all', array('view', 1));
  }

  /**
   *
   */
  public function summary_title() {
    return t('Election results access');
  }

}
