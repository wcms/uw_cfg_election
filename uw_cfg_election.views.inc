<?php

/**
 * @file
 * Functions relating to Views.
 */

/**
 * Implements hook_views_default_views_alter().
 *
 * For documentation on overriding default views in code,
 *
 * @see https://www.drupal.org/node/1014774
 */
function uw_cfg_election_views_default_views_alter(&$views) {
  if (array_key_exists('election_candidates', $views)) {
    $views['election_candidates']->display['default']->display_options['style_plugin'] = 'list';
    $views['election_candidates']->display['default']->display_options['style_options']['default_row_class'] = FALSE;
    $views['election_candidates']->display['default']->display_options['style_options']['row_class_special'] = FALSE;
    $views['election_candidates']->display['default']->display_options['row_plugin'] = 'fields';
    $views['election_candidates']->display['default']->display_options['row_options']['default_field_elements'] = FALSE;

    /* Field: Election candidate: Name */
    $views['election_candidates']->display['default']->display_options['fields']['candidate_name']['id'] = 'candidate_name';
    $views['election_candidates']->display['default']->display_options['fields']['candidate_name']['table'] = 'field_data_candidate_name';
    $views['election_candidates']->display['default']->display_options['fields']['candidate_name']['field'] = 'candidate_name';
    $views['election_candidates']->display['default']->display_options['fields']['candidate_name']['label'] = '';
    $views['election_candidates']->display['default']->display_options['fields']['candidate_name']['element_label_colon'] = FALSE;
    $views['election_candidates']->display['default']->display_options['fields']['candidate_name']['click_sort_column'] = 'title';
    $views['election_candidates']->display['default']->display_options['fields']['candidate_name']['settings'] = array(
      'format' => 'default',
      'markup' => 0,
      'output' => 'default',
      'multiple' => 'default',
      'multiple_delimiter' => ', ',
      'multiple_and' => 'text',
      'multiple_delimiter_precedes_last' => 'never',
      'multiple_el_al_min' => '3',
      'multiple_el_al_first' => '1',
    );

    $views['election_candidates']->display['embed_candidates_per_election']->display_options['style_plugin'] = 'list';
    $views['election_candidates']->display['embed_candidates_per_election']->display_options['style_options']['grouping'][0]['rendered'] = 0;

    $views['election_candidates']->display['embed_candidates_per_election']->display_options['style_options']['default_row_class'] = FALSE;
    $views['election_candidates']->display['embed_candidates_per_election']->display_options['style_options']['row_class_special'] = FALSE;

    $views['election_candidates']->display['embed_candidates_per_election']->display_options['row_plugin'] = 'fields';
    $views['election_candidates']->display['embed_candidates_per_election']->display_options['row_options']['default_field_elements'] = FALSE;

    $views['election_candidates']->display['embed_candidates_per_election']->display_options['fields']['title']['exclude'] = TRUE;

    /* Field: Election candidate: Name */
    $views['election_candidates']->display['embed_candidates_per_election']->display_options['fields']['candidate_name']['id'] = 'candidate_name';
    $views['election_candidates']->display['embed_candidates_per_election']->display_options['fields']['candidate_name']['table'] = 'field_data_candidate_name';
    $views['election_candidates']->display['embed_candidates_per_election']->display_options['fields']['candidate_name']['field'] = 'candidate_name';
    $views['election_candidates']->display['embed_candidates_per_election']->display_options['fields']['candidate_name']['label'] = '';
    $views['election_candidates']->display['embed_candidates_per_election']->display_options['fields']['candidate_name']['element_label_colon'] = FALSE;
    $views['election_candidates']->display['embed_candidates_per_election']->display_options['fields']['candidate_name']['click_sort_column'] = 'title';
    $views['election_candidates']->display['embed_candidates_per_election']->display_options['fields']['candidate_name']['settings'] = array(
      'format' => 'default',
      'markup' => 0,
      'output' => 'default',
      'multiple' => 'default',
      'multiple_delimiter' => ', ',
      'multiple_and' => 'text',
      'multiple_delimiter_precedes_last' => 'never',
      'multiple_el_al_min' => '3',
      'multiple_el_al_first' => '1',
    );
  }
}
