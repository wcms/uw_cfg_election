<?php

/**
 * @file
 * Functions relating to access control.
 */

/**
 * Clean a string of WatIAM IDs and convert to an array.
 *
 * @param string $ids
 *   WatIAM IDs, one per line.
 *
 * @return array
 *   An array of WatIAM IDs.
 */
function uw_cfg_election_watiam_ids_clean($ids) {
  $ids = check_plain($ids);
  $ids = strtolower($ids);
  // Split into array on any EOL character.
  $ids = preg_split('/\R/', $ids);
  // Remove leading and trailing whitespace.
  $ids = array_map('trim', $ids);
  // Remove empty.
  $ids = array_filter($ids);
  sort($ids);
  return $ids;
}

/**
 * Load from the database the voters' list for an election post.
 *
 * @param int $post_id
 *   The election post ID.
 *
 * @return array
 *   An array of WatIAM IDs.
 */
function uw_cfg_election_voters_list_load($post_id) {
  return db_select('uw_cfg_election_voters_list_entry', 'vle')
    ->fields('vle', ['watiam_id'])
    ->condition('post_id', $post_id)
    ->orderBy('watiam_id')
    ->execute()
    ->fetchCol();
}

/**
 * Return the number of people on a voters' list for an election post.
 *
 * @param int $post_id
 *   The election post ID.
 *
 * @return int
 *   The number of entries on the voters' list.
 */
function uw_cfg_election_voters_list_count($post_id) {
  $query = db_select('uw_cfg_election_voters_list_entry', 'vle');
  $query->addExpression('COUNT(*)');
  return (int) $query
    ->condition('post_id', $post_id)
    ->execute()
    ->fetch()
    ->expression;
}

/**
 * Save to the database a new voters' list for an election post.
 *
 * Upload the new voters' list to a temp table and synchronize the permanent
 * table to the temp table.
 *
 * @param int $post_id
 *   The election post ID.
 * @param array $watiam_ids
 *   An array of WatIAM IDs.
 */
function uw_cfg_election_voters_list_save($post_id, array $watiam_ids) {
  $txn = db_transaction();

  // Create an empty temp table.
  $temp = db_query_temporary('SELECT watiam_id FROM uw_cfg_election_voters_list_entry WHERE FALSE');
  // Insert the voters' list into the temp table.
  $query = db_insert($temp)
    ->fields(['watiam_id']);
  foreach ($watiam_ids as $watiam_id) {
    $query->values(['watiam_id' => $watiam_id]);
  }
  $query->execute();

  // Delete from database any voters not on the new voters' list.
  $watiam_ids_subquery = db_select($temp, 'temp')
    ->fields('temp');
  $query = db_delete('uw_cfg_election_voters_list_entry')
    ->condition('post_id', $post_id)
    ->condition('watiam_id', $watiam_ids_subquery, 'NOT IN')
    ->execute();

  // Add to the database any voters not listed there. In PostgreSQL, replace the
  // entire "ON DUPLICATE KEY UPDATE" line with "ON CONFLICT DO NOTHING".
  $query = db_query("INSERT INTO uw_cfg_election_voters_list_entry
    SELECT :post_id, watiam_id FROM $temp
    ON DUPLICATE KEY UPDATE post_id = post_id",
    [':post_id' => $post_id]);
}

/**
 * Check if a person is on the voters' list for an election post.
 *
 * @param int $post_id
 *   The election post ID.
 * @param string $watiam_id
 *   The user's WatIAM ID.
 *
 * @return bool
 *   TRUE if the WatIAM ID is on the voters' list, FALSE otherwise.
 */
function uw_cfg_election_voters_list_check($post_id, $watiam_id) {
  $query = db_select('uw_cfg_election_voters_list_entry', 'vle');
  $query->addExpression(1);
  return (bool) $query
    ->condition('post_id', $post_id)
    ->condition('watiam_id', $watiam_id)
    ->execute()
    ->fetch();
}

/**
 * Election condition callback.
 *
 * @param object $post
 *   The election post object.
 * @param object $account
 *   The user account object.
 *
 * @see uw_cfg_election_election_condition_info()
 */
function uw_cfg_election_condition_voters_list_check(stdClass $post, stdClass $account) {
  return uw_cfg_election_voters_list_check($post->post_id, $account->name);
}
