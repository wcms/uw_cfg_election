<?php

/**
 * @file
 * Code for the uWaterloo Election feature.
 */

include_once 'uw_cfg_election.features.inc';
include_once 'uw_cfg_election.views.inc';
include_once 'uw_cfg_election.access.inc';

/**
 * Implements hook_menu().
 */
function uw_cfg_election_menu() {
  $items = array();
  // Dummy entry needed to make tabs appear.
  $items['elections/list'] = array(
    'title' => 'Elections',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => 1,
  );
  $items['elections/help'] = array(
    'title' => 'Election help',
    'page callback' => 'uw_cfg_election_help_tab',
    'access arguments' => array('administer elections'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 3,
  );
  return $items;
}

/**
 * Implements hook_custom_theme().
 */
function uw_cfg_election_custom_theme() {
  if (preg_match(',^(elections/add/.+|(election|election-post|election-candidate)/\d+/(edit|add-\w+))$,', current_path())) {
    return variable_get('admin_theme');
  }
}

/**
 * Implements hook_views_plugins().
 */
function uw_cfg_election_views_plugins() {
  return array(
    'access' => array(
      'election_results' => array(
        'title' => t('Vote results access'),
        'help' => t('The user has access to view the results and the results are available.'),
        'handler' => 'uw_cfg_election_plugin_access_election_results',
      ),
    ),
  );
}

/**
 * Access callback for viewing results per election.
 *
 * This calls election_results_access() for every post in the election and
 * returns TRUE only if all posts are TRUE, FALSE otherwise.
 *
 * @param string $op
 *   The operation being performed. One of 'view', 'update', or 'delete'.
 * @param object $election
 *   An election to check access for.
 * @param object $account
 *   The user to check for. Leave it to NULL to check for the global user.
 *
 * @return bool
 *   Whether access is allowed or not.
 */
function uw_cfg_election_results_access_all($op, stdClass $election, stdClass $account = NULL) {
  $election = election_load($election);
  foreach (election_post_load_by_election($election, TRUE, FALSE) as $post) {
    if (!election_results_access($op, $post, $account)) {
      return FALSE;
    }
  }
  return TRUE;
}

/**
 * Implements hook_election_condition_info().
 */
function uw_cfg_election_election_condition_info() {
  $conditions = array();
  $conditions['uw_cfg_election_condition_voters_list_check'] = [
    'name' => t("On voters' list"),
    'description' => t("The user is on the voters' list."),
    'user explanation' => t("You must be on the voters' list."),
  ];
  return $conditions;
}

/**
 * Implements hook_election_type_info_alter().
 *
 * Change the name of FPTP elections, removing "FPTP".
 */
function uw_cfg_election_election_type_info_alter(array &$types) {
  $types['fptp']['name'] = t('election');
}

/**
 * Page callback.
 */
function uw_cfg_election_help_tab() {
  $page = array();
  $page['content'] = array(
    '#markup' => '<h2>Kinds of votes</h2>
<p>The system supports two kinds of votes:</p>
<dl>
<dt>election</dt>
<dd>First-past-the-post (FPTP) election. The candidate(s) with the most votes wins.</dd>
<dt>referendum</dt>
<dd>Voters will answer one or more referendum questions.</dd>
</dl>
<h2>Steps to run a vote</h2>
<ol>
<li>Create the vote, either first-past-the-post or referendum. Each vote contains one or more election positions or referendum questions (a single vote cannot contain both kinds).</li>
<li>Create one or more election positions or referendum questions. For each one, you must provide a voters\' list consisting of WatIAM IDs. Choose the number of vacancies for each position (default 1).</li>
<li>Add candidates to each position.</li>
<li>Make the link which voters will use to find the vote pages. These links need to be either to the vote (election/[number]) or to the position or question (election-post/[number]).</li>
<li>Edit the vote to open voting (or use the schedule feature to have it open/close automatically).</li>
<li>People vote. Results cannot be seen while voting is open, but you can look at the statistics of how many people have voted.</li>
<li>Close voting.</li>
<li>In each position/question, there will be a Results tab showing how many votes each option received.</li>
</ol>',
  );
  return $page;
}

/**
 * Implements hook_theme_registry_alter().
 */
function uw_cfg_election_theme_registry_alter(array &$theme_registry) {
  // @see uw_cfg_election_election_vote_eligibility().
  $theme_registry['election_vote_eligibility']['function'] = 'uw_cfg_election_election_vote_eligibility';

  // Custom version of election/election_post/election-post.tpl.php.
  // Removes links on h3 election post titles.
  if (isset($theme_registry['election_post'])) {
    $module_path = drupal_get_path('module', 'uw_cfg_election');
    $theme_registry['election_post']['theme path'] = $module_path;
    $theme_registry['election_post']['template'] = $module_path . '/templates/election-post';
  }
}

/**
 * Overrides theme_election_vote_eligibility().
 *
 * This version replaces the normal login link with a CAS login link.
 */
function uw_cfg_election_election_vote_eligibility(array $variables) {
  return '<span class="election-vote-eligibility">'
    . str_replace('user/login', 'cas', $variables['eligibility'])
    . '</span>';
}

/**
 * Implements hook_entity_view_alter().
 *
 * Hide unused nominations message.
 */
function uw_cfg_election_election_view_alter(array &$build, $type) {
  if ($type === 'election') {
    unset($build['nstatus']);
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function uw_cfg_election_form_election_form_alter(array &$form, array &$form_state, $form_id) {
  // Hide unused components.
  $form['field_election_information_page']['#access'] = FALSE;
  $form['field_referendum_information_pag']['#access'] = FALSE;
  $form['nominations']['#access'] = FALSE;
  $form['redirect']['#access'] = FALSE;
  $form['metatags']['#access'] = FALSE;
  $form['presentation']['#access'] = FALSE;
  // Always use anonymous mode.
  $form['voting']['settings_anonymize']['#type'] = 'value';
  $form['voting']['settings_anonymize']['#value'] = TRUE;
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function uw_cfg_election_form_election_candidate_form_alter(array &$form, array &$form_state, $form_id) {
  // Hide unused components.
  $form['cstatus']['#access'] = FALSE;
  $form['user']['#access'] = FALSE;
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function uw_cfg_election_form_election_post_form_alter(array &$form, array &$form_state, $form_id) {
  // Hide unused parts of the UI.
  $items = [
    'redirect',
    'metatags',
    'role_access',
    'conditions',
  ];
  foreach ($items as $item) {
    $form[$item]['#access'] = FALSE;
  }

  // Disable collapse on form controls.
  $items = [
    'voting',
    'candidates_nominations',
  ];
  foreach ($items as $item) {
    $form[$item]['#collapsible'] = FALSE;
  }

  // Hide nomination-related UI.
  $form['candidates_nominations']['settings_candidate_type']['#type'] = 'value';
  $form['candidates_nominations']['settings_exclusive']['#type'] = 'value';
  $form['candidates_nominations']['nstatus_inheritance']['#type'] = 'value';
  // Hide Voting status.
  $form['voting']['vstatus_inheritance']['#type'] = 'value';
  // Always use radios/checkboxes on ballot.
  $form['voting']['settings_vote_form_type']['#type'] = 'value';
  $form['voting']['settings_vote_form_type']['#value'] = 'radios';
  // Require voters' list election condition.
  $form['conditions']['conditions']['#type'] = 'value';
  $form['conditions']['conditions']['#value'] = ['uw_cfg_election_condition_voters_list_check' => TRUE];

  // Component for voters' list.
  $form['voters_list'] = [
    '#title' => 'Voters\' list',
    '#description' => 'Paste in the list of voters. It must be plain text consisting of one WatIAM ID per line and nothing else.',
    '#type' => 'textarea',
    '#rows' => 20,
    '#weight' => 91,
    '#element_validate' => ['_uw_cfg_election_form_election_post_form_validate'],
    '#default_value' => implode("\n", uw_cfg_election_voters_list_load($form_state['post']->post_id)),
  ];

  // Form submit handler.
  $form['buttons']['submit']['#submit'][] = '_uw_cfg_election_election_post_form_submit';
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function uw_cfg_election_form_election_vote_form_alter(array &$form, array &$form_state, $form_id) {
  if (isset($form['ballot_paper']['#description'])) {
    $form['ballot_paper']['#description'] = str_replace('Candidates are displayed in a random order.', 'Display order of candidates will vary.', $form['ballot_paper']['#description']);
  }
}

/**
 * Custom form API #element_validate handler.
 */
function _uw_cfg_election_form_election_post_form_validate(array $element, array &$form_state, array $form) {
  $ids = uw_cfg_election_watiam_ids_clean($form_state['values']['voters_list']);

  // Validate list.
  $invalid = [];
  // Validate against regular expression.
  foreach ($ids as $uid) {
    if (!preg_match('/^[a-z][a-z0-9-]{0,7}$/', $uid)) {
      // substr() limits the length of the error message.
      $invalid[] = substr($uid, 0, 100);
    }
    // Limit to 100 invalid.
    if (count($invalid) === 100) {
      break;
    }
  }

  // If all pass regex validation, validate against WatIAM database.
  if (!$invalid) {
    $ch = curl_init('https://api.data.uwaterloo.ca/api/v1/watiampersons/validate?limit=100');
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    $headers = [];
    $headers[] = 'Accept: application/json';
    $headers[] = 'Content-Type: application/json';
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($ids));

    $invalid = curl_exec($ch);
    $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    if ($status === 200 && is_string($invalid)) {
      $invalid = json_decode($invalid);
    }
    else {
      form_set_error('voters_list', t('Unable to use watiampersons API to validate WatIAM IDs. Status: @status. Error @errno: @error', [
        '@status' => $status,
        '@errno' => curl_errno($ch),
        '@error' => curl_error($ch),
      ]));
      // Error gets set here, so prevent later error.
      $invalid = FALSE;
    }

    curl_close($ch);
  }

  if ($invalid) {
    // Limit error message to first 100 invalid.
    $invalid = array_slice($invalid, 0, 100);
    form_set_error('voters_list', t('Invalid WatIAM ID: @uid.', ['@uid' => implode(', ', $invalid)]));
  }

  // Save array version of voters' list as form value.
  $form_state['values']['voters_list'] = $ids;
}

/**
 * Custom form API #submit handler.
 */
function _uw_cfg_election_election_post_form_submit(array $form, array &$form_state) {
  // Save the voters' list if it changed.
  $voters_list_current = uw_cfg_election_watiam_ids_clean($form['voters_list']['#default_value']);
  $voters_list_new = $form_state['values']['voters_list'];
  if ($voters_list_new !== $voters_list_current) {
    uw_cfg_election_voters_list_save($form_state['post']->post_id, $voters_list_new);
    drupal_set_message('Updated voters\' list.', 'status');
  }
  else {
    drupal_set_message('Voters\' list unchanged.', 'status');
  }
}

/**
 * Implements hook_preprocess_HOOK().
 */
function uw_cfg_election_preprocess_election_statistics(&$variables) {
  // Remove "Top 10 user agents" and "Top 10 IP addresses" from statistics page.
  unset($variables['content']['top_agents_table']);
  unset($variables['content']['top_ips_table']);

  if ($variables['content']['posts_table']) {
    // Adjust voters column title.
    $variables['content']['posts_table']['#header']['voters'] = 'Voted';

    // Add count of eligible voters.
    $variables['content']['posts_table']['#header'] = array_merge(
      array_slice($variables['content']['posts_table']['#header'], 0, 1, TRUE),
      ['eligible_voters' => 'Eligible to vote'],
      array_slice($variables['content']['posts_table']['#header'], 1, NULL, TRUE)
    );
    foreach ($variables['content']['posts_table']['#rows'] as $post_id => &$row) {
      $row = array_merge(
        array_slice($row, 0, 1, TRUE),
        ['eligible_voters' => uw_cfg_election_voters_list_count($post_id)],
        array_slice($row, 1, NULL, TRUE)
      );
    }
  }
}

/**
 * Implements hook_election_voting_open().
 */
function uw_cfg_election_election_voting_open($election_id, $scheduled) {
  $election = election_load($election_id);
  watchdog('election', 'Voting opened @how for @type %title (@id).', array(
    '@how' => $scheduled ? 'by schedule' : 'manually',
    '@type' => $election->type_info['name'],
    '%title' => $election->title,
    '@id' => $election->election_id,
  ));
}

/**
 * Implements hook_election_voting_close().
 */
function uw_cfg_election_election_voting_close($election_id, $scheduled) {
  global $user;

  $election = election_load($election_id);
  watchdog('election', 'Voting closed @how for @type %title (@id).', array(
    '@how' => $scheduled ? 'by schedule' : 'manually',
    '@type' => $election->type_info['name'],
    '%title' => $election->title,
    '@id' => $election->election_id,
  ));
  // Send voting results to the returning officer(s).
  if (!empty($election->field_returning_officer['und'])) {
    $uids = array_column($election->field_returning_officer['und'], 'uid');

    // Impersonate administrator so that Views always has the permission to
    // access results. https://www.drupal.org/node/218104
    $original_user = $user;
    $old_state = drupal_save_session();
    drupal_save_session(FALSE);
    $user = user_load(1);

    foreach (user_load_multiple($uids) as $account) {
      foreach (['results', 'audit-trail'] as $email_key) {
        drupal_mail('uw_cfg_election', $email_key, $account->mail, user_preferred_language($account), ['election' => $election]);
      }
    }

    // End user impersonation.
    $user = $original_user;
    drupal_save_session($old_state);
  }
}

/**
 * Implements hook_mail().
 */
function uw_cfg_election_mail($key, &$message, $params) {
  switch($key) {
    case 'results':
      $election = $params['election'];
      $langcode = $message['language']->language;
      $message['subject'] = t('Voting results for @election', ['@election' => $election->title], ['langcode' => $langcode]);
      $posts = election_post_load_by_election($election);
      foreach ($posts as $post) {
        $message['body'][] = '<h2>' . t('Results: %post', array('%post' => $post->title)) . '</h2>';
        $element = election_results_view($post);
        $message['body'][] = render($element);
      }
      break;
    case 'audit-trail':
      $election = $params['election'];
      $langcode = $message['language']->language;
      $message['subject'] = t('Voting audit trail for @election', ['@election' => $election->title], ['langcode' => $langcode]);
      $message['params']['plaintext'] = views_embed_view('election_audit_trail', 'views_data_export_1', $election->election_id);
      break;
  }
}

/**
 * Implements hook_default_elysia_cron_rules().
 */
function uw_cfg_election_default_elysia_cron_rules() {
  $cron_rules = array();

  $cron_rule = new stdClass();
  $cron_rule->disabled = FALSE; /* Edit this to true to make a default cron_rule disabled initially */
  $cron_rule->api_version = 1;
  $cron_rule->name = 'election_cron_cron';
  $cron_rule->disable = NULL;
  $cron_rule->rule = '*/5 * * * *';
  $cron_rule->weight = NULL;
  $cron_rule->context = NULL;
  $cron_rules['election_cron_cron'] = $cron_rule;

  return $cron_rules;
}
