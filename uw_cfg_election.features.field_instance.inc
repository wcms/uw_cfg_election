<?php

/**
 * @file
 * uw_cfg_election.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function uw_cfg_election_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'election-fptp-field_body_no_summary'.
  $field_instances['election-fptp-field_body_no_summary'] = array(
    'bundle' => 'fptp',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'election',
    'field_name' => 'field_body_no_summary',
    'label' => 'Body',
    'required' => 0,
    'settings' => array(
      'better_formats' => array(
        'allowed_formats' => array(
          'filtered_html' => 0,
          'full_html' => 'full_html',
          'plain_text' => 0,
          'single_page_remote_events' => 0,
          'uw_tf_basic' => 0,
          'uw_tf_comment' => 0,
          'uw_tf_conference' => 0,
          'uw_tf_contact' => 0,
          'uw_tf_standard' => 'uw_tf_standard',
          'uw_tf_standard_sidebar' => 0,
          'uw_tf_standard_site_footer' => 0,
          'uw_tf_standard_wide' => 'uw_tf_standard_wide',
        ),
        'allowed_formats_toggle' => 1,
        'default_order_toggle' => 0,
        'default_order_wrapper' => array(
          'formats' => array(
            'filtered_html' => array(
              'weight' => 0,
            ),
            'full_html' => array(
              'weight' => -6,
            ),
            'plain_text' => array(
              'weight' => 10,
            ),
            'single_page_remote_events' => array(
              'weight' => 0,
            ),
            'uw_tf_basic' => array(
              'weight' => 0,
            ),
            'uw_tf_comment' => array(
              'weight' => 0,
            ),
            'uw_tf_conference' => array(
              'weight' => 0,
            ),
            'uw_tf_contact' => array(
              'weight' => -5,
            ),
            'uw_tf_standard' => array(
              'weight' => -10,
            ),
            'uw_tf_standard_sidebar' => array(
              'weight' => -8,
            ),
            'uw_tf_standard_site_footer' => array(
              'weight' => -7,
            ),
            'uw_tf_standard_wide' => array(
              'weight' => -9,
            ),
          ),
        ),
      ),
      'entity_translation_sync' => FALSE,
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'label_help_description' => '',
        'rows' => 20,
      ),
      'type' => 'text_textarea',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'election-fptp-field_returning_officer'.
  $field_instances['election-fptp-field_returning_officer'] = array(
    'bundle' => 'fptp',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Returning officer(s) will receive the results by email when the election is closed. They must be users on this site before they can be added here.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'user_reference',
        'settings' => array(),
        'type' => 'user_reference_default',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'election',
    'field_name' => 'field_returning_officer',
    'label' => 'Returning officer(s)',
    'required' => 0,
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'user_reference',
      'settings' => array(
        'autocomplete_match' => 'contains',
        'autocomplete_path' => 'user_reference/autocomplete',
        'label_help_description' => '',
        'size' => 60,
      ),
      'type' => 'user_reference_autocomplete',
      'weight' => 3,
    ),
  );

  // Exported field_instance: 'election-referendum-field_body_no_summary'.
  $field_instances['election-referendum-field_body_no_summary'] = array(
    'bundle' => 'referendum',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'election',
    'field_name' => 'field_body_no_summary',
    'label' => 'Body',
    'required' => 0,
    'settings' => array(
      'better_formats' => array(
        'allowed_formats' => array(
          'filtered_html' => 0,
          'full_html' => 'full_html',
          'plain_text' => 0,
          'single_page_remote_events' => 0,
          'uw_tf_basic' => 0,
          'uw_tf_comment' => 0,
          'uw_tf_conference' => 0,
          'uw_tf_contact' => 0,
          'uw_tf_standard' => 'uw_tf_standard',
          'uw_tf_standard_sidebar' => 0,
          'uw_tf_standard_site_footer' => 0,
          'uw_tf_standard_wide' => 'uw_tf_standard_wide',
        ),
        'allowed_formats_toggle' => 1,
        'default_order_toggle' => 0,
        'default_order_wrapper' => array(
          'formats' => array(
            'filtered_html' => array(
              'weight' => 0,
            ),
            'full_html' => array(
              'weight' => -6,
            ),
            'plain_text' => array(
              'weight' => 10,
            ),
            'single_page_remote_events' => array(
              'weight' => 0,
            ),
            'uw_tf_basic' => array(
              'weight' => 0,
            ),
            'uw_tf_comment' => array(
              'weight' => 0,
            ),
            'uw_tf_conference' => array(
              'weight' => 0,
            ),
            'uw_tf_contact' => array(
              'weight' => -5,
            ),
            'uw_tf_standard' => array(
              'weight' => -10,
            ),
            'uw_tf_standard_sidebar' => array(
              'weight' => -8,
            ),
            'uw_tf_standard_site_footer' => array(
              'weight' => -7,
            ),
            'uw_tf_standard_wide' => array(
              'weight' => -9,
            ),
          ),
        ),
      ),
      'entity_translation_sync' => FALSE,
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'label_help_description' => '',
        'rows' => 20,
      ),
      'type' => 'text_textarea',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'election-referendum-field_returning_officer'.
  $field_instances['election-referendum-field_returning_officer'] = array(
    'bundle' => 'referendum',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Returning officer(s) will receive the results by email when the election is closed. They must be users on this site before they can be added here.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'user_reference',
        'settings' => array(),
        'type' => 'user_reference_default',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'election',
    'field_name' => 'field_returning_officer',
    'label' => 'Returning officer(s)',
    'required' => 0,
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'user_reference',
      'settings' => array(
        'autocomplete_match' => 'contains',
        'autocomplete_path' => 'user_reference/autocomplete',
        'label_help_description' => '',
        'size' => 60,
      ),
      'type' => 'user_reference_autocomplete',
      'weight' => 3,
    ),
  );

  // Exported field_instance: 'election_candidate-candidate-candidate_name'.
  $field_instances['election_candidate-candidate-candidate_name'] = array(
    'bundle' => 'candidate',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'name',
        'settings' => array(
          'format' => 'default',
          'markup' => 0,
          'multiple' => 'default',
          'multiple_and' => 'text',
          'multiple_delimiter' => ', ',
          'multiple_delimiter_precedes_last' => 'never',
          'multiple_el_al_first' => 1,
          'multiple_el_al_min' => 3,
          'output' => 'default',
        ),
        'type' => 'name_formatter',
        'weight' => 2,
      ),
      'full' => array(
        'label' => 'inline',
        'module' => 'name',
        'settings' => array(
          'format' => 'default',
          'markup' => 0,
          'multiple' => 'default',
          'multiple_and' => 'text',
          'multiple_delimiter' => ', ',
          'multiple_delimiter_precedes_last' => 'never',
          'multiple_el_al_first' => 1,
          'multiple_el_al_min' => 3,
          'output' => 'default',
        ),
        'type' => 'name_formatter',
        'weight' => 2,
      ),
      'teaser' => array(
        'label' => 'inline',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'election_candidate',
    'field_name' => 'candidate_name',
    'label' => 'Name',
    'required' => 1,
    'settings' => array(
      'component_css' => '',
      'component_layout' => 'default',
      'components' => array(),
      'credentials_inline' => 0,
      'entity_translation_sync' => FALSE,
      'field_type' => array(
        'credentials' => 'text',
        'family' => 'text',
        'generational' => 'select',
        'given' => 'text',
        'middle' => 'text',
        'title' => 'select',
      ),
      'generational_field' => 'select',
      'inline_css' => array(
        'credentials' => '',
        'family' => '',
        'generational' => '',
        'given' => '',
        'middle' => '',
        'title' => '',
      ),
      'labels' => array(
        'credentials' => '',
        'family' => '',
        'generational' => '',
        'given' => '',
        'middle' => '',
        'title' => '',
      ),
      'minimum_components' => array(),
      'override_format' => 'default',
      'show_component_required_marker' => 0,
      'size' => array(
        'credentials' => 35,
        'family' => 20,
        'generational' => 5,
        'given' => 20,
        'middle' => 20,
        'title' => 6,
      ),
      'title_display' => array(
        'credentials' => 'description',
        'family' => 'description',
        'generational' => 'description',
        'given' => 'description',
        'middle' => 'description',
        'title' => 'description',
      ),
      'title_field' => 'select',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'name',
      'settings' => array(),
      'type' => 'name_widget',
      'weight' => 1,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Body');
  t('Name');
  t('Returning officer(s)');
  t('Returning officer(s) will receive the results by email when the election is closed. They must be users on this site before they can be added here.');

  return $field_instances;
}
